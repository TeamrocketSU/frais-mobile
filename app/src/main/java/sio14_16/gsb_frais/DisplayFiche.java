package sio14_16.gsb_frais;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.loopj.android.http.JsonHttpResponseHandler;
import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.R.attr.lines;

public class DisplayFiche extends AppCompatActivity {

    Toolbar toolbar;
    TextView name;
    TextView montant;
    TextView montant_valid;
    TextView etat;
    ListView list_frais_forfait;
    ListView list_frais_hors_forfait;
    TextView commentaire;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_fiche);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        name = (TextView) findViewById(R.id.name);
        montant = (TextView) findViewById(R.id.montant);
        montant_valid = (TextView) findViewById(R.id.montant_valid);
        etat = (TextView) findViewById(R.id.etat);
        list_frais_forfait = (ListView) findViewById(R.id.list_frais_forfait);
        list_frais_hors_forfait = (ListView) findViewById(R.id.list_frais_hors_forfait);
        commentaire = (TextView) findViewById(R.id.commentaire);

        Intent i = getIntent();
        Integer id = i.getIntExtra("id", -1);
        if(id != -1){
            try {
                RestApiClient.get("fiches/" + id.toString() + "/detail", null, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            name.setText(response.getString("name"));
                            montant.setText(response.getString("montant") + "€");
                            montant_valid.setText(response.getString("montantValide") + "€");
                            etat.setText(response.getString("etat"));
                            commentaire.setText(response.getString("commentaire"));

                            JSONArray lines;
                            ListAdapter adapter;

                            lines = response.getJSONArray("les_lignes_forfaits");
                            adapter = formatListView(lines);
                            list_frais_forfait.setAdapter(adapter);

                            lines = response.getJSONArray("les_lignes_hors_forfaits");
                            adapter = formatListView(lines);
                            list_frais_hors_forfait.setAdapter(adapter);
                            setListViewHeightBasedOnChildren(list_frais_forfait);
                            setListViewHeightBasedOnChildren(list_frais_hors_forfait);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private ListAdapter formatListView(JSONArray lines) {
        final List<HashMap<String, String>> list = new ArrayList<>();
        HashMap<String, String> element;
        JSONObject line;
        for(int i = 0 ; i < lines.length(); i++) {
            element = new HashMap<>();
            try {
                line = lines.getJSONObject(i);
                element.put("name", line.getString("name"));
                element.put("description", line.getString("montant") + "€ - " + line.getString("etat"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            list.add(element);
        }

        return new SimpleAdapter(this,
                list,
                android.R.layout.simple_list_item_2,
                new String[] {"name", "description"},
                new int[] {android.R.id.text1, android.R.id.text2 }
        );
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, Toolbar.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
