package sio14_16.gsb_frais;

import cz.msebera.android.httpclient.Header;
import org.json.*;
import com.loopj.android.http.*;

public class RestApiClient {
    private static final String BASE_URL = "http://frais.sio14-16.fr/app_dev.php/api/";
    private static AsyncHttpClient client = new AsyncHttpClient();
    private static Boolean isAuth = false;

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) throws Exception {
        if(!isAuth){
            throw new Exception("Must be login before");
        }

        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) throws Exception {
        if(!isAuth){
            throw new Exception("Must be login before");
        }
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void auth(String username, String password){

        RequestParams params = new RequestParams();
        params.put("grant_type", "password");
        params.put("client_id", "1_3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4");
        params.put("client_secret", "4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k");
        params.put("username", username);
        params.put("password", password);

        SyncHttpClient SyncClient = new SyncHttpClient();
        SyncClient.post(getAbsoluteUrl("oauth/v2/token"), params,  new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    String accessToken = response.getString("access_token");
                    client.addHeader("Authorization", "Bearer "+ accessToken);
                    isAuth = true;
                } catch (JSONException e) {
                }

            }
        });
    }

    public static Boolean isAuth(){
        return isAuth;
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
