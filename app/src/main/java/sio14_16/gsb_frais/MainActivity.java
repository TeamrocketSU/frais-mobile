package sio14_16.gsb_frais;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;
import com.loopj.android.http.JsonHttpResponseHandler;
import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ListView view;
    DrawerLayout drawer;
    Toolbar toolbar;
    NavigationView navigationView;
    TextView title;
    ImageView icon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        view = (ListView) findViewById(R.id.listView);
        title = (TextView) findViewById(R.id.title);
        icon = (ImageView) findViewById(R.id.icon);

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        showListFiche(this);
    }

    private void showListFiche(final Context context) {
        title.setText("Liste des fiche de frais");
        try {
            RestApiClient.get("fiches", null, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {

                    final List<HashMap<String, String>> list = new ArrayList<>();
                    HashMap<String, String> element;

                    for (int i = 0; i < response.length(); i++) {
                        element = new HashMap<>();
                        try {
                            String description = response.getJSONObject(i).getString("montantValide") + "€/" +
                                    response.getJSONObject(i).getString("montant") + "€ - " +
                                    response.getJSONObject(i).getString("etat");

                            element.put("id", response.getJSONObject(i).getString("id"));
                            element.put("name", response.getJSONObject(i).getString("name"));
                            element.put("description", description);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        list.add(element);
                    }

                    ListAdapter adapter = new SimpleAdapter(context,
                            list,
                            android.R.layout.simple_list_item_2,
                            new String[]{"name", "description"},
                            new int[]{android.R.id.text1, android.R.id.text2}
                    );
                    view.setAdapter(adapter);

                    view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            Intent intent = new Intent(MainActivity.this, DisplayFiche.class);
                            try {
                                intent.putExtra("id", response.getJSONObject(position).getInt("id"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            startActivity(intent);
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        view.setAdapter(null);
        title.setText("");

        if (id == R.id.nav_history) {
            showListFiche(this);

        } else if (id == R.id.nav_call) {
            showAnnuaire(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showAnnuaire(MainActivity mainActivity) {
        title.setText("Annuaire des comptables");
        final Context context = this;
        try {
            RestApiClient.get("contactable/comptable", null, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {

                    final List<HashMap<String, String>> list = new ArrayList<>();
                    HashMap<String, String> element;

                    for (int i = 0; i < response.length(); i++) {
                        element = new HashMap<>();
                        try {
                            element.put("name", response.getJSONObject(i).getString("nom") + " " + response.getJSONObject(i).getString("prenom"));
                            element.put("description", response.getJSONObject(i).getString("ville") + " | " + response.getJSONObject(i).getString("number"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        list.add(element);
                    }

                    ListAdapter adapter = new SimpleAdapter(context,
                            list,
                            android.R.layout.simple_list_item_2,
                            new String[]{"name", "description"},
                            new int[]{android.R.id.text1, android.R.id.text2}
                    );
                    view.setAdapter(adapter);

                    view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            try {
                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:"+response.getJSONObject(position).getString("number")));
                                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(callIntent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
